# SOFTWARES UTILIZADOS
* Appium 1.1.1
* Java 1.8
* Eclipse Oxygen 4.7
* MacOS Sierra 10.12.3

# 1. APLICATIVO ALVO DO TESTE

## 1.A. GERAR ARQUIVO .app
0. Abra o XCode (Cmd + Espaço, digite xcode e pressione Enter).
0. Selecione o projeto no Menu.
0. Clique em Product -> Destination -> Ipad Pro (ou qualquer outro simulador).
0. Clique em Product -> Build.
0. O arquivo .app estará gerado no diretório Products.
0. Copie o arquivo para o diretório desejado.
0. Para instalar no simulador, basta arrastar o arquivo para a janela do simulador.

## 1.B. EXPORTAR ARQUIVO IPA
0. Autentique-se em https://developer.apple.com/.
0. Clique no menu lateral Certificates, Identifiers & Profiles.
0. Clique no menu lateral Devices e escolha entre iPad ou iPhone.
0. Clique no botão +.
0. Preencha Name com um nome arbitrário e UDID com o ID do dispositivo em que o arquivo IPA será instalado.
0. O ID do dispositivo é obtido por meio do XCode -> Window -> Devices, clique no dispositivo e pegue o valor Identifier.
0. Abra o XCode (Cmd + Espaço, digite xcode e pressione Enter).
0. Selecione o projeto no Menu.
0. Clique em Product -> Archive. O XCode gerará o build.
0. Clique no botão Export.
0. Clique no botão Export for Development Deployment e clique no botão Next.
0. Selecione a conta na qual você se autenticou no passo 1 e clique no botão Choose.
0. Selecione Export one app for all compatible devices e clique em Next.
0. Tire a seleção de Rebuild from bitcode e clique em Next. (Caso não queira otimizar o pacote. Esse processo leva vários minutos).
0. Escolha o nome do arquivo em Export As e o diretório em Where.
0. Abra o iTunes.
0. Clique no ícone do iPhone ou iPad.
0. Clique no menu Apps.
0. Arraste o arquivo IPA do Finder para o iTunes, na listagem dos aplicativos.
0. Clique no botão Aplicar.
0. O aplicativo deverá ser instalado.
 

# 2. INSTALAÇÃO DO FERRAMENTAL

## 2.1. INSTALAÇÃO APPIUM
0. Acesse https://github.com/appium/appium-desktop/releases/.
0. Clique no link appium-desktop-x.x.x.dmg (x é o número da última versão).
0. Abra o Finder.
0. Clique em Transferências.
0. Duplo clique em appium-desktop-x.x.x.dmg.
0. Após o arquivo ser extraído, uma nova janela do Finder será aberta contendo um atalho do diretório Applications e o ícone do Appium. Arraste o ícone para o diretório.

## 2.2. INSTALAÇÃO JAVA
## 2.3. INSTALAÇÃO ECLIPSE

#3. CÓDIGO PARA AUTOMAÇÃO

## 3.1 PROJETO NO ECLIPSE
0. Abra o eclipse (Cmd + Espaço, digite eclipse e pressione Enter).
0. Clique no menu File -> New -> Maven Project.
0. Marque a caixa Create a simple project (skip archetype selection).
0. Clique no botão Next.
0. Preencha os campos Group Id e Artifact Id.
0. Clique no botão Finish.
0. Abra o arquivo pom.xml localizado na raiz do projeto.
0. Insira os elementos abaixo e salve o arquivo.
```xml
<properties>
<maven.compiler.target>1.8</maven.compiler.target>
<maven.compiler.source>1.8</maven.compiler.source>
<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
</properties>
<dependencies>
<dependency>
<groupId>io.appium</groupId>
<artifactId>java-client</artifactId>
<version>4.1.2</version>
</dependency>
</dependencies>
```
0. Clique com o botão inverso do mouse no ícone do projeto (na view Project Explorer, painel no lado esquerdo do eclipse).
0. Selecione Maven -> Update Project.
0. Uma janela será apresentada. Clique em no botão OK. O eclipse baixará a API do Appium e configurará o projeto para utilizar o Java 8.
0. Clique com o botão secundário no diretório src/main/java, selecione New -> Class.
0. Preencha o pacote e o nome da classe.
0. Clique no botão OK.
0. Inclua o seguinte método.
```java
public static void main(String... args) {
DesiredCapabilities capabilities = new DesiredCapabilities();
capabilities.setCapability("platformVersion", "10.3"); //Especifica a versão do iOS do simulador
capabilities.setCapability("deviceName", "iPad (5th generation)"); //Especifica o modelo do simulador do iOS. A lista de valores possíveis pode ser obtida com o comando 
instruments -s devices
 
capabilities.setCapability("app", "/Users/rsi3338/Desktop/Coffee Ratio.app"); //Caminho absoluto da aplicação que será instalada.
 
URL url = new URL("http://localhost:4723/wd/hub"); //URL para registro no Appium.
 

//Script Automação
IOSDriver<?> iosDriver = new IOSDriver<>(url, capabilities);

iosDriver.quit(); 
 
}
```
0. Inicie o Appium (Cmd + Espaço, digite appium e pressione Enter).
0. Clique no botão Start Server v.1.6.5.
0. De volta ao eclipse, execute o código clicando no menu Run -> Run. 
0. A automação irá iniciar o simulador, instalará o arquivo .app no simulador, e executará o script de automação.


## 3.2 INSPECIONAR ELEMENTOS
0. Inicie o Appium (Cmd + Espaço, digite appium e pressione Enter).
0. Clique no botão Start Server v.1.6.5.
0. Clique no botão Start New Session.
0. Clique na guia Automatic Server.
0. Clique na guia Desired Capabilities.
0. Preencha de acordo com o tutorial EXECUÇÃO NO SIMULADOR ou EXECUÇÃO NO DISPOSITIVO REAL.
0. Clique no botão Start Session. O inspetor será iniciado com a aplicação em exibição.
0. Clique no elemento desejado.
0. No painel Selected Element copie o Xpath. 


## 3.3. EXECUÇÃO
Caso a execução se dê no simulador, um arquivo .app será necessário, caso seja no dispositivo real (iPad ou iPhone) o ipa deverá ser instalado via iTunes ou seu caminho especificado na capability app.
 

### 3.3.A. EXECUÇÃO NO SIMULADOR
```java
      DesiredCapabilities capabilities = new DesiredCapabilities();
      capabilities.setCapability("platformVersion", "10.3");
      capabilities.setCapability("deviceName", "iPad (5th generation)"); 
 ```

####Com arquivo .app NÃO instalado
```java
      capabilities.setCapability("app", "/Users/rsi3338/Desktop/Coffee Ratio.app");
```
###Com arquivo .app instalado
```java 
      capabilities.setCapability("bundleId", "jmp9c.Coffee-Ratio");
```
 

### 3.3.B. EXECUÇÃO NO DISPOSITIVO REAL
```java
      DesiredCapabilities capabilities = new DesiredCapabilities();
      capabilities.setCapability("deviceName", "testDevice");
 
      capabilities.setCapability("automationName", "XCUITest");
      capabilities.setCapability("udid", "auto");
```
####Com arquivo IPA não instalado
```java 
      capabilities.setCapability("app", "/Users/rsi3338/Desktop/Coffee Ratio.ipa");
```
####Com arquivo IPA NÃO instalado
```java   
      capabilities.setCapability("bundleId", "jmp9c.Coffee-Ratio");
```
      
##OPÇÕES DE DEPLOY DE ARQUIVO .ipa

| Deployment | Descritivo | Licença |
| -----------|------------|---------|
|iOS Store | Para subir a aplicação para a App Store. | Apple Developer Program |
|Ad Hoc | Pode ser instalado nos dispositivos com UDID cadastrados no perfil de quem exportar a aplicação. | Apple Developer Program |
|Enterprise | Aplicação pode ser instalada apenas para os funcionários de uma empresa. | Apple Developer Enterprise Program (299 dólares anuais)
|Development | Pode ser instalado apenas nos dispositivos dos membros do time de quem exportar a aplicação. | Apple Developer Program |

__Referência:__ [Apple Help](http://help.apple.com/xcode/mac/9.0/#/dev31de635e5)