package br.com.rsinet.poc_appium.ios;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseListenerImpl implements MouseListener {

	
	private int x;
	private int y;

	public int getLastClickedX() {
		return x;
	}
	
	public int getLastClickedY() {
		return y;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		x = e.getX(); 
		y = e.getY();
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	public void resetXY() {
		x = 0;
		y = 0;
	}

}
