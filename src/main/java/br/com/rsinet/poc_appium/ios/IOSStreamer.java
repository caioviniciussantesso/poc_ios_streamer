package br.com.rsinet.poc_appium.ios;

import java.awt.Image;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.appium.java_client.ios.IOSDriver;

public class IOSStreamer {
	private static final Logger logger = LoggerFactory.getLogger(IOSStreamer.class);
	private static final int UI_WIDTH = 650;
	private static final int UI_HEIGHT = 870;

	static{
		logger.info("started.");
	}
	
	public static void main(String... args) {
		IOSDriver<?> iosDriver = initIOSDriver();
		MouseListenerImpl mouseListener = new MouseListenerImpl();
		JLabel screenLabel = initUI(mouseListener, UI_WIDTH, UI_HEIGHT);

		Dimension screenSize = iosDriver.manage().window().getSize();
		double xRatio = screenSize.getWidth() / (double) UI_WIDTH;
		double yRatio = screenSize.getHeight() / (double) UI_HEIGHT;

		for (int i = 0; i < 1000; i++) {

			byte[] screenshot = iosDriver.getScreenshotAs(OutputType.BYTES);
			ImageIcon imageIcon = new ImageIcon(new ImageIcon(screenshot).getImage().getScaledInstance(UI_WIDTH,
					UI_HEIGHT - 22, Image.SCALE_DEFAULT));
			screenLabel.setIcon(imageIcon);

			int x = (int) (mouseListener.getLastClickedX() * xRatio);
			int y = (int) (mouseListener.getLastClickedY() * yRatio);

			if (x == 0 || y == 0)
				continue;

			logger.info("Clique na UI X:{} Y:{} \t Clique no dispositivo X:{} Y:{}", mouseListener.getLastClickedX(),
					mouseListener.getLastClickedY(), x, y);
			iosDriver.tap(1, x, y, 1);
			mouseListener.resetXY();
		}
		iosDriver.quit();
	}

	/**
	 * Inicia {@link WebDriver} para iOS.
	 * 
	 * @return
	 */
	public static IOSDriver<?> initIOSDriver() {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "testDevice");
		capabilities.setCapability("xcodeOrgId", "4FSBQD52AV");
		capabilities.setCapability("xcodeSigningId", "iPhone Developer");
		capabilities.setCapability("automationName", "XCUITest");
		capabilities.setCapability("udid", "auto");

		URL ipaLocation = ClassLoader.getSystemResource("Coffee Ratio.ipa");
		try {
			capabilities.setCapability("app", Paths.get(ipaLocation.toURI()).toString());
		} catch (URISyntaxException e) {
			logger.error("Exiting program...", e);
			System.exit(1);
		}

		URL url = null;
		try {
			url = new URL("http://localhost:4723/wd/hub");
		} catch (MalformedURLException e) {
			logger.error("Exiting program...", e);
			System.exit(1);
		}

		return new IOSDriver<>(url, capabilities);
	}

	/**
	 * Inicia Interface Gráfica.
	 * 
	 * @param mouseListener
	 * @return
	 */
	public static JLabel initUI(MouseListener mouseListener, int width, int height) {
		JFrame frame = new JFrame();
		frame.setTitle("iOS");
		frame.setSize(width, height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

		JLabel label = new JLabel();
		label.addMouseListener(mouseListener);
		frame.add(label);

		return label;
	}
}